#ifndef CODELABELDRAWER_H
#define CODELABELDRAWER_H

#include <QObject>
#include <QPainter>
#include <QPdfWriter>
#include <QPixmap>
#include <QStandardPaths>
#include <qrcodegen.hpp>

class CodeLabelDrawer : public QObject
{
    Q_OBJECT
public:
    explicit CodeLabelDrawer(QObject *parent = nullptr);

    QString path() const;
    void setPath(const QString &newPath);
    QString getDefaultFileName();

public slots:
    void generateLabelCollection(QStringList codes);
signals:

private:
    QPixmap _generateQrCodePixMap(QString content);
    QPixmap _generateLabel(QString code);

    QString _path = "";
};

#endif // CODELABELDRAWER_H

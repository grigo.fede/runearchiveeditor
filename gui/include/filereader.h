#ifndef FILEREADER_H
#define FILEREADER_H

#include <QObject>
#include "archivedatamodel.h"

/**
 * @brief The FileReader class
 * @details This class will provide ways to read and write files, exposing them also to the qml engine.
 * @todo the idea is to implement support for the following formats: xlsx,json,xml,csv
 *
 */
class FileReader : public QObject
{
    Q_OBJECT
public:
    explicit FileReader(QObject *parent = nullptr);
    explicit FileReader(ArchiveDataModel *model, QObject *parent = nullptr);

    Q_INVOKABLE void readXslx(const QUrl &path);
    void writeXslx(const QUrl &path);
    void writeCodeList(QStringList codes, QStringList names, const QUrl &path);

    QString fileName() const;
    void setFileName(const QString &newFileName);

    QString folderPath() const;
    void setFolderPath(const QString &newFolderPath);

signals:

private:
    ArchiveDataModel *_dataModel;

    QString _fileName = "";
    QString _folderPath = "";
};

#endif // FILEREADER_H

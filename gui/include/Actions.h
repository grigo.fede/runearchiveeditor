#ifndef ACTIONS_H
#define ACTIONS_H

#include <QDesktopServices>
#include <QObject>

#include "CodeLabelDrawer.h"
#include "archivedatamodel.h"
#include "filereader.h"

class Actions : public QObject
{
    Q_OBJECT
public:
    explicit Actions(QObject *parent = nullptr);
    explicit Actions(ArchiveDataModel *model, QObject *parent = nullptr);
    explicit Actions(ArchiveDataModel *model, FileReader *fileReader, QObject *parent = nullptr);

public slots:
    void handleSelection(int i);
    void readXslx(const QUrl &path);
    void saveXslx(const QUrl &path);
    void saveXslx();
    void generateLabels();
    void generateLabels(const QUrl &path);
    void undoSelection();
    void selectAllNewBooks();
    void selectAll();

    void addNewBook(QString category, QString title, int code, int copy);
    void addNewBook(QString category, QString title, int code, int copy, QString page);

    // getters for qml
    QUrl getCurrentFilePath();
    QUrl getLabelFilePath();
    QUrl getCurrentFileFolderPath();
    int getNextRegistrationCode(QString category);
    int getNextRegistrationCopy(QString category, int code);
signals:

private:
    ArchiveDataModel *_dataModel;
    FileReader *_fileReader;
    CodeLabelDrawer _codeLabelDrawer{this};
};

#endif // ACTIONS_H

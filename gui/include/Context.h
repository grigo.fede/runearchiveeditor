#ifndef CONTEXT_H
#define CONTEXT_H

#include "Actions.h"
#include "QmlOption.h"
#include "archiveEntry.h"
#include "archivedatamodel.h"
#include "filereader.h"

#include <QGuiApplication>
#include <QObject>
#include <QQmlApplicationEngine>

class Context : public QObject
{
    Q_OBJECT
public:
    explicit Context(QGuiApplication *parent = nullptr);

signals:

private:
    QQmlApplicationEngine _engine;

    ArchiveDataModel _archiveDataModel{this};
    FileReader _fileReader{&_archiveDataModel, this};
    Actions _actions{&_archiveDataModel, &_fileReader, this};

    QCustomListModel<QmlOptionPLUS> _options{this};
    void _registerQmlTypes();
};

#endif // CONTEXT_H

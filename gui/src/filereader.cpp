#include "../include/filereader.h"

// [0] include QXlsx headers
#include <QDir>
#include <QStandardPaths>
#include "QmlOption.h"
#include "qnamespace.h"
#include "xlsxcellrange.h"
#include "xlsxchart.h"
#include "xlsxchartsheet.h"
#include "xlsxdocument.h"
#include "xlsxrichstring.h"
#include "xlsxworkbook.h"
#include <archiveEntry.h>

using namespace QXlsx;

FileReader::FileReader(QObject *parent)
    : QObject{parent}
    , _dataModel(new ArchiveDataModel(this))
{}

FileReader::FileReader(ArchiveDataModel *model, QObject *parent)
    : QObject{parent}
    , _dataModel(model)
{
    _folderPath = QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation)
                  + QDir::separator();
    _fileName = "GDR Archive.xlsx";
}

void FileReader::readXslx(const QUrl &path)
{
    std::vector<ArchiveHistoryEntry *> list;
    QStringList categories;
    Document doc(path.path());
    int invalidCount = 0;

    QString filename = path.fileName();
    QString folderPath = path.path().remove(filename);

    setFolderPath(folderPath);
    setFileName(filename);

    if (doc.load()) // load excel file
    {
        _dataModel->pageList()->clear();
        _dataModel->getItems()->clear();
        _dataModel->selectionList()->clear();
        for (QString sheetName : doc.sheetNames()) {
            QmlOption *opt = new QmlOption(sheetName, _dataModel->pageList()->size());
            opt->setFormattedLabel(opt->label().replace("&", "&&"));
            _dataModel->pageList()->append(opt);

            doc.selectSheet(sheetName);

            int rowCount = doc.dimension().lastRow();
            int colCount = doc.dimension().lastColumn();
            int printendlines = 1;

            if (colCount >= 3 && rowCount >= 2) {
                for (int i = doc.dimension().firstRow() + 1; i < doc.dimension().lastRow() + 1;
                     i++) {
                    Cell *codeCell = doc.cellAt(i, 1); // get cell pointer.
                    Cell *nameCell = doc.cellAt(i, 2);
                    Cell *noteCell = doc.cellAt(i, 3);
                    Cell *oldCodeCell = doc.cellAt(i, 4);

                    if (codeCell != NULL && nameCell != NULL) {
                        QVariant codeVar = codeCell->readValue();
                        QVariant nameVar = nameCell->readValue();
                        QVariant noteVar = noteCell != NULL ? noteCell->readValue() : "";
                        if (codeVar.isValid() && nameVar.isValid() && noteVar.isValid()) {
                            ArchiveHistoryEntry *readModel = new ArchiveHistoryEntry(
                                codeVar.toString());

                            readModel->setName(nameVar.toString());
                            readModel->setNote(noteVar.toString());
                            readModel->setItemIndex(_dataModel->getItems()->size());
                            readModel->setPreviousCode(oldCodeCell->readValue().toString());
                            readModel->setWithLabel(true);
                            readModel->setSheetName(sheetName);
                            _dataModel->getItems()->append(readModel);

                            QString categorycandidate = "";
                            if (sheetName == "Riviste")
                                int tt = 1;
                            if (!readModel->isValid()) {
                                invalidCount++;
                                if (oldCodeCell->readValue().toString().size() > 6)
                                    categorycandidate = readModel->previousCode().first(
                                        readModel->previousCode().size() - 6);

                            } else {
                                categorycandidate = readModel->getFullCode().first(
                                    readModel->getFullCode().size() - 6);
                            }
                            if (!categories.contains(categorycandidate)) {
                                //qDebug() << category;
                                categories.append(categorycandidate);
                            }
                        }
                    }
                }
            }
        }
    }

    for (int i = 0; i < categories.size(); i++) {
        QmlOption *opt = new QmlOption(categories.at(i), i);
        opt->setFormattedLabel(opt->label().replace("&", "&&"));
        _dataModel->getCategories()->append(opt);
    }
    _dataModel->standardItemSort();

    auto prop = dynamic_cast<ArchiveHistoryEntry *>(_dataModel->getItems()->get(0));
    auto s = prop->staticMetaObject.propertyCount();
    qInfo() << "Volumes" << list.size();
    qInfo() << "Categories" << _dataModel->getCategories()->size();
    qInfo() << "invalid volums" << invalidCount;
}

void FileReader::writeXslx(const QUrl &path)
{
    QXlsx::Document xlsxW;

    QString filename = path.fileName();
    QString folderPath = path.path().remove(filename);

    int pageEntryCount = 0;

    setFolderPath(folderPath);
    setFileName(filename);
    QString sheetName;
    QXlsx::Format formatCell;

    for (int s = 0; s < _dataModel->pageList()->size(); s++) {
        pageEntryCount = 0;
        auto prop = dynamic_cast<QmlOption *>(_dataModel->pageList()->get(s));
        sheetName = prop->label();
        xlsxW.addSheet(sheetName);
        xlsxW.selectSheet(sheetName);

        xlsxW.setColumnWidth(1, 15);
        xlsxW.setColumnWidth(4, 15);
        xlsxW.setColumnWidth(2, 35);
        xlsxW.setColumnWidth(3, 30);
        formatCell.setPatternBackgroundColor(QColor(Qt::GlobalColor::green));
        xlsxW.setRowFormat(1, formatCell);
        xlsxW.write(1, 1, "Codice");
        xlsxW.write(1, 2, "Nome");
        xlsxW.write(1, 3, "Note");
        xlsxW.write(1, 4, "Vecchio Codice");
        xlsxW.write(1, 5, "Etichettato");

        for (int i = 0; i < _dataModel->getItems()->size(); i++) {
            auto entry = dynamic_cast<ArchiveHistoryEntry *>(_dataModel->getItems()->get(i));

            if (entry->sheetName() == sheetName) {
                if (!entry->isValid()) {
                    formatCell.setPatternBackgroundColor(QColor(Qt::red));
                    xlsxW.setRowFormat(pageEntryCount + 2, formatCell);
                }
                xlsxW.write(pageEntryCount + 2, 1, entry->getPrintCode());
                xlsxW.write(pageEntryCount + 2, 2, entry->name());
                xlsxW.write(pageEntryCount + 2, 3, entry->note());
                xlsxW.write(pageEntryCount + 2, 4, entry->previousCode());
                pageEntryCount++;
            }
        }
    }
    xlsxW.saveAs(path.path());
}

void FileReader::writeCodeList(QStringList codes, QStringList names, const QUrl &path)
{
    if (codes.size() != names.size()) {
        qWarning() << "Errore nella stampa dell'elenco dei codici nuovi";
        return;
    }
    QXlsx::Document xlsxW;
    xlsxW.addSheet("Recap");
    xlsxW.selectSheet("Recap");
    for (int i = 0; i < codes.size(); i++) {
        xlsxW.write(i + 1, 1, codes.at(i));
        xlsxW.write(i + 1, 2, names.at(i));
    }
    xlsxW.saveAs(path.path());
}

QString FileReader::fileName() const
{
    return _fileName;
}

void FileReader::setFileName(const QString &newFileName)
{
    _fileName = newFileName;
}

QString FileReader::folderPath() const
{
    return _folderPath;
}

void FileReader::setFolderPath(const QString &newFolderPath)
{
    _folderPath = newFolderPath;
}

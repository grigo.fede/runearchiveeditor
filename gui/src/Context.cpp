#include "../include/Context.h"
#include "Actions.h"
#include "QCustomListModel.h"
#include "QmlOption.h"
#include "archiveEntry.h"
#include "filereader.h"
#include "qqml.h"

#include <QQuickStyle>
#include <QSettings>

Context::Context(QGuiApplication *parent)
    : QObject{parent}
{
    QSettings settings("Grigosoft", "RuneArchiveEditor");

    QQuickStyle::setStyle("Fusion");
    _registerQmlTypes();

    const QUrl url("qrc:/RuneArchiveEditor/qml/Main.qml");
    QObject::connect(
        &_engine,
        &QQmlApplicationEngine::objectCreationFailed,
        parent,
        []() { QCoreApplication::exit(-1); },
        Qt::QueuedConnection);
    _engine.load(url);
    if (_engine.rootObjects().isEmpty()) {
        QCoreApplication::exit(-1);
    }
}

void Context::_registerQmlTypes()
{
    qmlRegisterSingletonType<FileReader>(
        "RAE", 1, 0, "FileReader", [&](QQmlEngine *, QJSEngine *) -> QObject * {
            QQmlEngine::setObjectOwnership(&_fileReader, QQmlEngine::CppOwnership);
            return &_fileReader;
        });
    qmlRegisterSingletonType<ArchiveDataModel>(
        "RAE", 1, 0, "ArchiveModel", [&](QQmlEngine *, QJSEngine *) -> QObject * {
            QQmlEngine::setObjectOwnership(&_archiveDataModel, QQmlEngine::CppOwnership);
            return &_archiveDataModel;
        });
    qmlRegisterSingletonType<Actions>("RAE",
                                      1,
                                      0,
                                      "Actions",
                                      [&](QQmlEngine *, QJSEngine *) -> QObject * {
                                          QQmlEngine::setObjectOwnership(&_actions,
                                                                         QQmlEngine::CppOwnership);
                                          return &_actions;
                                      });

    qmlRegisterSingletonType<QCustomListModelBase>(
        "RAE", 1, 0, "MyModel", [&](QQmlEngine *, QJSEngine *) -> QObject * {
            QQmlEngine::setObjectOwnership(&_options, QQmlEngine::CppOwnership);
            return &_options;
        });
    _options.append(new QmlOptionPLUS("ciao", 2));
}

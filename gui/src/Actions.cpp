#include "../include/Actions.h"
#include "archiveEntry.h"
#include "qdesktopservices.h"

#include <QDir>

Actions::Actions(QObject *parent)
    : QObject{parent}
    , _dataModel{nullptr}
{}

Actions::Actions(ArchiveDataModel *model, QObject *parent)
    : QObject{parent}
    , _dataModel{model}
{}

Actions::Actions(ArchiveDataModel *model, FileReader *fileReader, QObject *parent)
    : QObject{parent}
    , _dataModel{model}
    , _fileReader{fileReader}
{}

void Actions::handleSelection(int i)
{
    _dataModel->handleSelection(i);
}

void Actions::readXslx(const QUrl &path)
{
    _dataModel->getItems()->clear();
    _fileReader->readXslx(path);
    _codeLabelDrawer.setPath(_fileReader->folderPath());
}

void Actions::saveXslx(const QUrl &path)
{
    _fileReader->writeXslx(path);
}

void Actions::saveXslx()
{
    _fileReader->writeXslx(_fileReader->folderPath() + _fileReader->fileName());
}

void Actions::generateLabels()
{
    auto selection = _dataModel->selectionList()->toList();
    auto path = _fileReader->folderPath() + _codeLabelDrawer.getDefaultFileName();
    QStringList codeList;

    if (selection.size() > 0) {
        for (int i : selection) {
            auto data = _dataModel->findByItemIndex(i);
            codeList.append(data->getPrintCode());
        }
        _codeLabelDrawer.setPath(path);
        _codeLabelDrawer.generateLabelCollection(codeList);
        QDesktopServices::openUrl("file://" + path);

        QStringList nameList;
        for (QString code : codeList) {
            nameList.append(_dataModel->findByFullCode(code)->name());
        }
        _fileReader->writeCodeList(codeList,
                                   nameList,
                                   _fileReader->folderPath() + "Lista Nuovi Manuali.xlsx");
    }
}

void Actions::generateLabels(const QUrl &path)
{
    auto selection = _dataModel->selectionList()->toList();
    QStringList codeList;
    if (selection.size() > 0) {
        for (int i : selection) {
            auto data = _dataModel->findByItemIndex(i);
            codeList.append(data->getPrintCode());
        }

        _codeLabelDrawer.setPath(path.path());
        _codeLabelDrawer.generateLabelCollection(codeList);

        QDesktopServices::openUrl(path);
    }
}

void Actions::undoSelection()
{
    _dataModel->selectionList()->clear();
    emit _dataModel->selectionListChanged();
}

void Actions::selectAllNewBooks()
{
    for (int i = 0; i < _dataModel->getItems()->size(); i++) {
        ArchiveHistoryEntry *data = dynamic_cast<ArchiveHistoryEntry *>(
            _dataModel->getItems()->get(i));
        if (!data->withLabel() && !_dataModel->selectionList()->contains(data->itemIndex())) {
            _dataModel->selectionList()->append(data->itemIndex());
        }
    }
    emit _dataModel->selectionListChanged();
}

void Actions::selectAll()
{
    for (int i = 0; i < _dataModel->getItems()->size(); i++) {
        ArchiveHistoryEntry *data = dynamic_cast<ArchiveHistoryEntry *>(
            _dataModel->getItems()->get(i));
        _dataModel->selectionList()->append(data->itemIndex());
    }
    emit _dataModel->selectionListChanged();
}

void Actions::addNewBook(QString category, QString title, int code, int copy)
{
    _dataModel->addNewItem(category, title, code, copy);
}

void Actions::addNewBook(QString category, QString title, int code, int copy, QString page)
{
    _dataModel->addNewItem(category, title, code, copy, page);
}

QUrl Actions::getCurrentFilePath()
{
    QUrl path = _codeLabelDrawer.path();
    return path;
}

QUrl Actions::getLabelFilePath() {}

QUrl Actions::getCurrentFileFolderPath()
{
    QUrl path = _codeLabelDrawer.path();
    return path.path().remove(path.fileName());
}

int Actions::getNextRegistrationCode(QString category)
{
    int maxCode = 0;
    for (int i = 0; i < _dataModel->getItems()->size(); i++) {
        ArchiveHistoryEntry *data = dynamic_cast<ArchiveHistoryEntry *>(
            _dataModel->getItems()->get(i));

        if (data->category() == category && data->registrationCode() > maxCode) {
            maxCode = data->registrationCode();
        }
    }
    return maxCode + 1;
}

int Actions::getNextRegistrationCopy(QString category, int code)
{
    int maxCode = 0;
    for (int i = 0; i < _dataModel->getItems()->size(); i++) {
        ArchiveHistoryEntry *data = dynamic_cast<ArchiveHistoryEntry *>(
            _dataModel->getItems()->get(i));

        if (data->category() == category && data->registrationCode() == code
            && data->copyNumber() > maxCode) {
            maxCode = data->copyNumber();
        }
    }
    return maxCode + 1;
}

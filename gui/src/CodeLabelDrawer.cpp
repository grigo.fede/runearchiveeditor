#include "../include/CodeLabelDrawer.h"
#include "qdir.h"

#include <QPainterPath>
#include <QPixmap>
#include <QStaticText>

CodeLabelDrawer::CodeLabelDrawer(QObject *parent)
    : QObject{parent}
{
    _path = QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation) + QDir::separator();
}

QString CodeLabelDrawer::path() const
{
    return _path;
}

void CodeLabelDrawer::setPath(const QString &newPath)
{
    _path = newPath;
}

QString CodeLabelDrawer::getDefaultFileName()
{
    return "GDR Labels.pdf";
}

void CodeLabelDrawer::generateLabelCollection(QStringList codes)
{
    QString filePath = _path;
    QPdfWriter pdfLabelWriter(filePath);

    pdfLabelWriter.setPageSize(QPageSize(QPageSize::A4));
    pdfLabelWriter.setTitle("labels");
    QMarginsF tmpMargins(0, 4.5, 0, 4.5);
    pdfLabelWriter.setPageMargins(tmpMargins);
    QPainter labelPainter(&pdfLabelWriter);
    labelPainter.setRenderHint(QPainter::Antialiasing);

    auto pdfDpcm = pdfLabelWriter.width() / 21;
    int labelPrintWidth = 7 * pdfDpcm;
    int labelPrintHeight = 3.6 * pdfDpcm;
    int pageCounter = 0;
    for (int i = 0; i < codes.size(); i++) {
        if (i != 0 && i % 24 == 0) {
            pdfLabelWriter.newPage();
            pageCounter++;
        }
        labelPainter.drawPixmap({(int) labelPrintWidth * (i % 3),
                                 (int) labelPrintHeight * ((i - (pageCounter * 24)) / 3),
                                 labelPrintWidth,
                                 labelPrintHeight},
                                _generateLabel(codes[i]));
    }
}

QPixmap CodeLabelDrawer::_generateQrCodePixMap(QString content)
{
    const qrcodegen::QrCode qr1 = qrcodegen::QrCode::encodeText(content.toStdString().c_str(),
                                                                qrcodegen::QrCode::Ecc::HIGH);
    int multiplier = 6;
    int offset = 2;
    int internalSize = qr1.getSize() * multiplier;
    int imageSize = internalSize + 2 * offset * multiplier;
    QPixmap image(imageSize, imageSize);
    image.fill(Qt::transparent);

    QPainter qrpainter(&image);
    qrpainter.setPen(Qt::black);
    qrpainter.setBrush(Qt::black);

    //painter.drawRect(50, 50, 3, 3);

    for (int y = 0; y < qr1.getSize(); y++) {
        for (int x = 0; x < qr1.getSize(); x++) {
            if (qr1.getModule(x, y))
                qrpainter.drawRect((x + offset) * multiplier,
                                   (y + offset) * multiplier,
                                   multiplier,
                                   multiplier);
        }
    }
    qrpainter.end();
    return image;
}

QPixmap CodeLabelDrawer::_generateLabel(QString code)
{
    const int labelWidth = 700;
    const int labelHeight = 360;
    QPixmap gen(labelWidth, labelHeight);

    QImage logo("RuneArchiveEditor/qml/resources/logoRuneDelLupo.png");
    gen.fill(Qt::transparent);
    QPainter pixmapPainter(&gen);
    pixmapPainter.setRenderHint(QPainter::Antialiasing, true);
    pixmapPainter.setRenderHint(QPainter::LosslessImageRendering, true);
    pixmapPainter.setRenderHint(QPainter::TextAntialiasing, true);
    pixmapPainter.setPen(Qt::black);
    pixmapPainter.setBrush(Qt::black);

    QPainterPath path;
    auto borderRect = QRectF(0.5, 0.5, labelWidth - 0.5, labelHeight - 0.5);

    //path.addRect(borderRect);

    QPen pen(Qt::black, 2);
    QBrush brush(Qt::transparent);
    pixmapPainter.setBrush(brush);
    pixmapPainter.setPen(pen);
    pixmapPainter.fillPath(path, Qt::transparent);
    pixmapPainter.drawPath(path);

    // big logo
    QRectF target(labelWidth - 270, 30, 250, 250);
    QRectF source(0.0, 0.0, 512.0, 512.0);
    pixmapPainter.drawImage(target, logo, source);

    // rim logo
    target.setRect(labelWidth / 2 - 45 / 2, labelHeight - 45 - 20, 45, 45);
    pixmapPainter.drawImage(target, logo, source);

    // front logo
    target.setRect(130, 20, 70, 70);
    pixmapPainter.drawImage(target, logo, source);

    QFont _font;
    _font.setPointSize(32);
    _font.setWeight(QFont::Medium);
    _font.setFamily("League Spartan");
    pixmapPainter.setFont(_font);

    pixmapPainter.rotate(-90);
    QStaticText text(code);
    pixmapPainter.drawStaticText(-285, 328, text);
    pixmapPainter.rotate(90);

    _font.setPointSize(34);
    pixmapPainter.drawStaticText(labelWidth - 270, labelHeight - 45 - 25, text);

    auto qrImage = _generateQrCodePixMap(code);
    target.setRect(40.0, 95.0, 250.0, 250.0);
    source.setRect(0, 0, qrImage.width(), qrImage.height());
    pixmapPainter.drawPixmap(target, qrImage, source);

    pixmapPainter.end();

    //gen.toImage().save("qualcosa.png");
    return gen.copy();
}

#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQuickStyle>
#include "Context.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    Context context{&app};

    return app.exec();
}

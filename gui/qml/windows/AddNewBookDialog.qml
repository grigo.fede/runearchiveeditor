import QtQuick 2.15
import QtQuick.Controls
import QtQuick.Layouts
import QtQuick.LocalStorage

import RAE

Dialog {

    id: dialog
    title: "Add a new task"

    property string categoryName: ""

    onAboutToShow: {
        nameField.text = ""
        codeField.text = ""
        copyField.value = 1
    }

    Column {
        spacing: 10
        RowLayout {
            spacing: 10
            Label {
                text: "Name*:"
            }
            TextField {
                id:nameField

                Layout.preferredWidth: 400
            }
        }
        RowLayout {
            spacing: 10
            Label {
                text: "Category*:"
            }
            ComboBox{
                id:comboType
                model: ArchiveModel.categories
                textRole: "formattedLabel"
                Component.onCompleted: {
                    comboType.currentIndex = -1
                }
                onActivated:  {
                    //console.log(ArchiveModel.categories.get(comboType.currentIndex).label)
                    categoryName = ArchiveModel.categories.get(comboType.currentIndex).label
                }
            }
        }
        RowLayout {
            spacing: 10
            Label {
                text: "Section:"
            }
            ComboBox{
                id:comboPage
                model: ArchiveModel.pageList
                textRole: "label"

                Component.onCompleted: {
                    if(currentIndex===-1){
                        currentIndex=0
                    }
                }
            }
        }
        RowLayout {
            spacing: 10
            Label {
                text: "Registration Code: "

            }
            TextField{
                id: codeField
                Layout.preferredWidth: 100
                inputMethodHints: Qt.ImhDigitsOnly
                maximumLength: 4
                validator: IntValidator{
                    bottom: 0
                    top: 9999
                }
            }

            Button{
                id:autogenButton
                text: "Auto"
                onClicked: {
                    codeField.text = Actions.getNextRegistrationCode(comboType.currentText)
                }
            }
        }
        RowLayout {
            spacing: 10
            Label {
                text: "Copy number:"
            }
            SpinBox {
                id: copyField
                Layout.preferredWidth: 100
                from: 1
                to: 9
                stepSize: 1
                editable: true
            }
            Button{
                id:autogenCopyButton
                text: "Auto"
                onClicked: {
                    copyField.value = Actions.getNextRegistrationCopy(comboType.currentText,parseInt(codeField.text))
                }
            }
        }
        Row {
            spacing: 10

            Button {
                text: "Cancel"
                onClicked: {
                    dialog.close()
                }
            }

            Button {
                text: "Add Book"
                onClicked: {
                    if (nameField.text !== "") {
                        console.log("add")
                        Actions.addNewBook( categoryName,nameField.text, parseInt(codeField.text), copyField.value,comboPage.currentText)
                        dialog.close()
                    }
                }
            }
        }
    }
}

import QtQuick
import QtQuick.Controls

import RAE

Dialog {
    id:dialog
    title: "Add Category"

    Column {
        spacing: 10

        Label {
            text: "Name"
            anchors.left: parent.left
            anchors.leftMargin: 10
        }
        TextField {
            id: nameField
            font.capitalization: Font.AllUppercase
        }
        Row {
            spacing: 10
            Button {
                text: "Cancel"
                onClicked: {
                    dialog.close()
                }
            }

            Button {
                text: "Add"
                onClicked: {
                    if (nameField.text !== "") {
                        console.log("close")
                        ArchiveModel.addNewCategory(nameField.text)
                        dialog.close()
                    }
                }
            }
        }
    }
}

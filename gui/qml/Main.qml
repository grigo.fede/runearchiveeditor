import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import QtQuick.Dialogs
import QtCore
import "windows"

import RAE


ApplicationWindow {
    width: 840
    height: 480
    visible: true
    title: qsTr("Rune Archive Editor")
    flags: Qt.Window

    visibility: Window.Maximized

    SystemPalette { id: sysPalette; colorGroup: SystemPalette.Active }

    menuBar: MenuBar {
        Menu {
            title: qsTr("File")
            MenuItem {
                text: qsTr("Open")
                action: Action {
                    onTriggered: {
                        fileDialog.open()
                    }
                }
            }
            MenuItem {
                text: qsTr("Save")
                action: Action{
                    onTriggered: {
                        Actions.saveXslx()
                    }
                }
            }
            MenuItem {
                text: qsTr("Save As")
            }
            MenuItem {
                text: qsTr("Exit")
            }
        }
    }

    header: ToolBar {
        RowLayout {
            anchors.fill:parent
            spacing: 10
            ToolButton {
                text: "New"
                action: Action {
                    onTriggered: {
                        console.log("New")

                        ArchiveModel.items.clear();
                    }
                }
            }
            ToolButton {
                text: "Open"
                action: Action {
                    onTriggered: {
                        console.log(StandardPaths.DocumentsLocation)
                        fileDialog.open()
                    }
                }
            }
            ToolButton {
                text: "Save"
                action: Action {
                    onTriggered: {
                        Actions.saveXslx()
                    }
                }

            }
            ToolButton {
                text: "Save As"
                action: Action {
                    onTriggered: {
                        Actions.getCurrentFilePath();
                        saveDialog.open()
                    }
                }
            }
            ToolButton {
                text: "New Category"
                action: Action{
                    onTriggered: {

                        addCategoryDialog.open()
                    }
                }
            }
            ToolButton {
                text: "New Book"
                action: Action{
                    onTriggered: {
                        addBookDialog.open()
                    }
                }
            }
            ToolButton {
                text: "Generate Labels"
                action: Action{
                    onTriggered: {
                        Actions.generateLabels();
                    }
                }
            }
            ToolButton {
                text: "Select new Books"
                action: Action{
                    onTriggered: {
                        Actions.selectAllNewBooks();
                    }
                }
            }
            ToolButton {
                text: "Undo Selection"
                action: Action{
                    onTriggered: {
                        Actions.undoSelection();
                    }
                }
            }

            ToolButton {
                text: "Exit"
            }
        }
    }

    StackView{
        id:stack
        initialItem: mainView
        anchors.fill: parent
    }

    Component {
        id: mainView

        ColumnLayout{

            anchors.fill: parent
            TabBar {
                id: bar
                Layout.fillWidth: true


               TabButton {
                   anchors.margins: 5
                    text: qsTr("All")
                }
               Repeater {
                    model: ArchiveModel.pageList
                    TabButton {
                        anchors.margins: 5
                        text: ArchiveModel.pageList.get(index)?.formattedLabel

                    }
                }
            }
            ListView {
                id: fruitModel
                Layout.fillHeight: true
                Layout.fillWidth: true
                Layout.margins: 10
                headerPositioning: ListView.OverlayHeader
                clip: true
                header:
                    Rectangle {
                    z: 3
                    anchors.left: parent.left
                    anchors.right: parent.right
                    height: 50
                    color:sysPalette.base
                    RowLayout{
                        anchors.left: parent.left
                        anchors.top: parent.top
                        anchors.bottom: parent.bottom
                        anchors.margins: 10
                        anchors.leftMargin: 10
                        spacing: 10

                        CheckBox{
                            id:checkBoxHeader
                            checked: false
                            action: Action {
                                onTriggered: {
                                    if(!checkBoxHeader.checked){
                                        Actions.selectAll()
                                        checkBoxHeader.checked=true
                                    }else{
                                        Actions.undoSelection()
                                        checkBoxHeader.checked=false
                                    }
                                }
                            }
                        }

                        Text {
                            id: headerName
                            text: qsTr("Titolo")
                            Layout.preferredWidth: 400
                            color: sysPalette.text
                            font.pointSize: 12
                            font.weight: 500
                        }
                        Text {
                            id: headerCategory
                            text: qsTr("Categoria")
                            Layout.preferredWidth: 120
                            color: sysPalette.text
                            font.pointSize: 12
                            font.weight: 500
                        }

                        Text {
                            id: headerregCode
                            text: qsTr("Codice")
                            Layout.preferredWidth: 100
                            color: sysPalette.text
                            font.pointSize: 12
                            font.weight: 500
                        }

                        Text {
                            id: copyCode
                            text: qsTr("Copia")
                            Layout.preferredWidth: 100
                            color: sysPalette.text
                            font.pointSize: 12
                            font.weight: 500
                        }
                        Text {
                            id: sezione
                            text: qsTr("Sezione")
                            Layout.preferredWidth: 120
                            color: sysPalette.text
                            font.pointSize: 12
                            font.weight: 500
                        }
                        Text {
                            id: previousCode
                            text: qsTr("Codice Precedente")
                            Layout.preferredWidth: 400
                            color: sysPalette.text
                            font.pointSize: 12
                            font.weight: 500
                        }
                    }
                }


                model: ArchiveModel.items

                ScrollBar.vertical: ScrollBar {
                    hoverEnabled: true
                    active: hovered || pressed
                    orientation: Qt.Vertical
                    size: fruitModel.height / fruitModel.height
                    anchors.top: parent.top
                    anchors.right: parent.right
                    anchors.bottom: parent.bottom
                }

                delegate: Rectangle{
                    id:delegate
                    width: fruitModel.width
                    height: bar.currentIndex==0 || ArchiveModel.pageList.get(bar.currentIndex-1)?.label==modelPageName ? 40:0
                    opacity: height==0?0:1
                    color: !model.withLabel? "green" :  model.category!="" && ArchiveModel.items.get(index).isValid() ? "lightgrey"
                                                                                                                      : "red"
                    border.color: "black"
                    border.width: 1
                    property string modelCategory: model.category
                    property string modelPageName: model.sheetName
                    property int delegateIndex: index

                    onModelPageNameChanged: {
                        for (var i = 0; i < ArchiveModel.pageList.size(); i++) {
                            if (ArchiveModel.pageList.get(i).label === modelPageName) {
                                comboPage.currentIndex = i;
                                return;
                            }
                        }
                        comboPage.currentIndex = 0;
                    }

                    onModelCategoryChanged: {
                        for (var i = 0; i < ArchiveModel.categories.size(); i++) {
                            if (ArchiveModel.categories.get(i).label === modelCategory) {
                                comboType.currentIndex = i;

                                return;
                            }
                        }
                        comboType.currentIndex = -1;
                    }

                    RowLayout{
                        anchors.left:parent.left
                        anchors.top:parent.top
                        anchors.bottom:parent.bottom
                        anchors.leftMargin: 10
                        spacing: 10

                        CheckBox{
                            id:checkBox
                            checked: ArchiveModel.selectionList.contains(model.itemIndex)
                            action: Action {
                                onTriggered: {
                                    ArchiveModel.items.get(index).isValid()
                                    ArchiveModel.handleSelection(model.itemIndex)
                                }
                            }
                            onCheckedChanged: {

                                // ArchiveModel.handleSelection(model.itemIndex)
                                // ArchiveModel.handleSelection(model.itemIndex+2)
                            }
                        }

                        TextField {
                            id: nameEditField
                            text: model.name

                            Layout.preferredWidth: 400
                            onAccepted: {
                                ArchiveModel.items.get(delegateIndex).name=text
                            }

                        }

                        // Text {
                        //     text:   model.category
                        //     Layout.preferredWidth: 100

                        // }
                        ComboBox{
                            id:comboType
                            model: ArchiveModel.categories
                            textRole: "label"

                            delegate: ItemDelegate {
                                width: comboType.width
                                highlighted: comboType.highlightedIndex === index
                                contentItem: Text {
                                    text: comboType.textRole
                                          ? (Array.isArray(comboType.model) ? modelData[comboType.textRole] : model[comboType.textRole])
                                          : modelData
                                    color: comboType.highlightedIndex === index ? sysPalette.highlightedText : sysPalette.text

                                    font.bold: comboType.currentIndex === index
                                    elide: Text.ElideRight
                                    verticalAlignment: Text.AlignVCenter
                                }

                            }

                            Component.onCompleted: {
                                for (var i = 0; i < ArchiveModel.categories.size(); i++) {
                                    if (ArchiveModel.categories.get(i).label === modelCategory) {
                                        comboType.currentIndex = i;
                                        return;
                                    }
                                }
                                comboType.currentIndex = -1;
                            }



                            onActivated: index => {
                                             let val=ArchiveModel.categories.get(index).label
                                             ArchiveModel.items.get(delegateIndex).category=val
                                         }

                            // onCurrentIndexChanged: {
                            // Connections {
                            //     target: CreateConfigurationModel
                            //     function onMasterTypeChanged() {
                            //         for (var i = 0; i < comboType.customItems.length; i++) {
                            //             if (comboType.customItems[i].value === CreateConfigurationModel.masterType) {
                            //                 comboType.currentIndex = i;
                            //                 break;
                            //             }
                            //         }
                            //     }
                            // }

                        }

                        TextField{
                            text:  model.registrationCode
                            Layout.preferredWidth: 100
                            inputMethodHints: Qt.ImhDigitsOnly
                            maximumLength: 4
                            validator: IntValidator{
                                bottom: 0
                                top: 9999
                            }
                            onAccepted: {
                                ArchiveModel.items.get(delegateIndex).registrationCode=text
                            }
                        }

                        SpinBox {
                            id: spinBox
                            value: model.copyNumber
                            Layout.preferredWidth: 100
                            from: 1
                            to: 9
                            stepSize: 1
                            editable: true
                            onValueChanged: {
                                ArchiveModel.items.get(delegateIndex).copyNumber=value
                            }
                        }

                        // Text {
                        //     text:   model.category!=""? ArchiveModel.items.get(delegateIndex).getFullCode()  : "Invalid"

                        //     Layout.preferredWidth: 100

                        // }


                        ComboBox{
                            id:comboPage
                            model: ArchiveModel.pageList
                            textRole: "label"

                            delegate: ItemDelegate {
                                width: comboPage.width
                                highlighted: comboPage.highlightedIndex === index
                                contentItem: Text {
                                    text: comboPage.textRole
                                          ? (Array.isArray(comboPage.model) ? modelData[comboPage.textRole] : model[comboPage.textRole])
                                          : modelData
                                    color: comboPage.highlightedIndex === index ? sysPalette.highlightedText : sysPalette.text
                                    // font: comboPage.font
                                    font.bold: comboPage.currentIndex === index
                                    elide: Text.ElideRight
                                    verticalAlignment: Text.AlignVCenter
                                }

                            }


                            Component.onCompleted: {
                                for (var i = 0; i < ArchiveModel.pageList.size(); i++) {
                                    if (ArchiveModel.pageList.get(i).label === modelPageName) {
                                        comboPage.currentIndex = i;
                                        return;
                                    }
                                }
                                comboPage.currentIndex = -1;
                            }

                            onActivated: index => {
                                             let val=ArchiveModel.pageList.get(index).label
                                             ArchiveModel.items.get(delegateIndex).sheetName=val

                                         }

                        }
                        Text {
                            text: model.previousCode

                            Layout.preferredWidth: 150
                        }
                        // Text {
                        //     text: model.sheetName

                        //     Layout.preferredWidth: 150
                        // }
                    }
                }
            }

        }

    }


    FileDialog{
        id: fileDialog
        title: qsTr("Open File")
        currentFolder: StandardPaths.standardLocations(StandardPaths.DocumentsLocation)[0]
        onAccepted: {
            console.log("Accepted")
            console.log(fileDialog.currentFile )
            Actions.readXslx(fileDialog.currentFile )
        }
        onRejected: {
            console.log("Rejected")
        }
    }

    FileDialog {
        id: saveDialog
        title: "Save File"
        currentFolder: StandardPaths.writableLocation(StandardPaths.DocumentsLocation)
        //currentFolder: StandardPaths.standardLocations(StandardPaths.DocumentsLocation)
        defaultSuffix: "xlsx"
        nameFilters: ["Excel files (*.xslx)", "All files (*)"]
        fileMode:FileDialog.SaveFile

        onAccepted: {
            // The user clicked "Save", fileUrl contains the selected file path
            console.log(selectedFile)
            var selectedFilePath = selectedFile//fileDialog.fileUrl.toString();
            console.log("Selected File: " + selectedFilePath);
            Actions.saveXslx(selectedFilePath);
            // Perform actions with the selected file path
            // For example, save content to the file
            // ...
        }

        onRejected: {
            // The user clicked "Cancel" or closed the dialog
            console.log("Dialog closed or canceled");
        }
    }

    AddNewCategoryDialog{
        id:addCategoryDialog
        anchors.centerIn: parent
    }

    AddNewBookDialog{
        id:addBookDialog
        anchors.centerIn: parent
    }
}

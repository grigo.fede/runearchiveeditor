#include "../include/archivedatamodel.h"
#include <QModelIndex>
#include <QQmlListProperty>
#include "QCustomListModel.h"
#include "QmlOption.h"
#include <archiveEntry.h>

ArchiveDataModel::ArchiveDataModel(QObject *parent)
    : QObject{parent}
    , m_items{new QCustomListModel<ArchiveHistoryEntry>(this)}
    , m_categories(new QCustomListModel<QmlOption>(this))
    , m_selectionList(new QmlIntegerModelList(this))
    , m_pageList(new QCustomListModel<QmlOption>(this))
    , m_currentCategory("")
    , m_proxyItems(new QSortFilterProxyModel())
{
}

QCustomListModel<ArchiveHistoryEntry> *ArchiveDataModel::getItems()
{
    return &m_items;
}

QCustomListModelBase *ArchiveDataModel::getCategories() const
{
    return m_categories;
}

void ArchiveDataModel::addNewCategory(QString label)
{
    auto candidate = label.toUpper();
    if (label != "") {
        for (int i = 0; i < m_categories->size(); i++) {
            auto tmp = dynamic_cast<QmlOption *>(m_categories->get(i));

            if (tmp->label() == candidate)
                return;
        }
        m_categories->append(new QmlOption(candidate, m_categories->size()));
    }
}

void ArchiveDataModel::addNewItem(QString category, QString title, int code, int copy)
{
    auto tmp = new ArchiveHistoryEntry();
    tmp->setCategory(category);
    tmp->setName(title);
    tmp->setRegistrationCode(code);
    tmp->setCopyNumber(copy);
    for (int i = 0; i < m_items.size(); i++) {
        auto candidate = dynamic_cast<ArchiveHistoryEntry *>(m_items.get(i));
        if (candidate->getPrintCode() == tmp->getPrintCode())
            // TODO: emit error signal here
            return;
    }
    tmp->setItemIndex(m_items.size());
    m_items.append(tmp);

    standardItemSort();
}

void ArchiveDataModel::addNewItem(QString category, QString title, int code, int copy, QString page)
{
    auto tmp = new ArchiveHistoryEntry();
    tmp->setCategory(category);
    tmp->setName(title);
    tmp->setRegistrationCode(code);
    tmp->setCopyNumber(copy);
    for (int i = 0; i < m_items.size(); i++) {
        auto candidate = dynamic_cast<ArchiveHistoryEntry *>(m_items.get(i));
        if (candidate->getPrintCode() == tmp->getPrintCode())
            // TODO: emit error signal here
            return;
    }
    tmp->setItemIndex(m_items.size());
    tmp->setSheetName(page);
    m_items.append(tmp);

    standardItemSort();
}

ArchiveHistoryEntry *ArchiveDataModel::findByItemIndex(int itemIndex)
{
    for (int i = 0; i < m_items.size(); i++) {
        auto tmp = dynamic_cast<ArchiveHistoryEntry *>(m_items.get(i));

        if (tmp->itemIndex() == itemIndex)
            return tmp;
    }
    return nullptr;
}

ArchiveHistoryEntry *ArchiveDataModel::findByFullCode(QString code)
{
    for (int i = 0; i < m_items.size(); i++) {
        auto tmp = dynamic_cast<ArchiveHistoryEntry *>(m_items.get(i));
        auto tmpCode = tmp->getFullCode();
        if (tmp->isCodeParsable(tmpCode) && tmpCode == code)
            return tmp;
    }
    return nullptr;
}

void ArchiveDataModel::standardItemSort()
{
    std::sort(getItems()->begin(),
              getItems()->end(),
              [](ArchiveHistoryEntry *a, ArchiveHistoryEntry *b) {
                  if (!a->withLabel() || !b->withLabel()) {
                      if (!a->withLabel() && !b->withLabel())
                          return a->name() < b->name();
                      if (!a->withLabel())
                          return true;
                      return false;
                  }
                  if (a->isValid() && b->isValid()) {
                      if (a->category() == b->category())
                          return a->getPrintCode() < b->getPrintCode();
                      return a->category() < b->category();
                  } else {
                      if (!a->isValid() && !b->isValid())
                          return a->name() < b->name();
                      if (!a->isValid())
                          return true;
                      return false;
                  }
              });
    emit getItems()->dataChanged(getItems()->index(0, 0),
                                 getItems()->index(getItems()->size() - 1, 0));
}

QmlIntegerModelList *ArchiveDataModel::selectionList()
{
    return m_selectionList;
}

void ArchiveDataModel::handleSelection(int i)
{
    if (m_selectionList->contains(i)) {
        m_selectionList->remove(i);
    } else {
        m_selectionList->append(new QmlInteger(i));
    }
    emit selectionListChanged();
}

QCustomListModel<QmlOption> *ArchiveDataModel::pageList() const
{
    return m_pageList;
}

QString ArchiveDataModel::currentCategory() const
{
    return m_currentCategory;
}

void ArchiveDataModel::setCurrentCategory(const QString &newCurrentCategory)
{
    if (m_currentCategory == newCurrentCategory)
        return;
    m_currentCategory = newCurrentCategory;
    emit currentCategoryChanged();
}

QSortFilterProxyModel *ArchiveDataModel::proxyItems() const
{
    return m_proxyItems;
}

void ArchiveDataModel::setProxyItems(QSortFilterProxyModel *newProxyItems)
{
    if (m_proxyItems == newProxyItems)
        return;
    m_proxyItems = newProxyItems;
    emit proxyItemsChanged();
}

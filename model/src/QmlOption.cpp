#include "../include/QmlOption.h"
#include "qobject.h"

QmlOption::QmlOption(QObject *parent)
    : QObject{parent}
{}

QmlOption::QmlOption(QString label, int value, QObject *parent)
    : QObject(parent)
    , m_label{label}
    , m_value{value}
    , m_formattedLabel{label}
{}

QString QmlOption::label() const
{
    return m_label;
}

void QmlOption::setLabel(const QString &newLabel)
{
    if (m_label == newLabel)
        return;
    m_label = newLabel;
    emit labelChanged();
}

int QmlOption::value() const
{
    return m_value;
}

void QmlOption::setValue(int newValue)
{
    if (m_value == newValue)
        return;
    m_value = newValue;
    emit valueChanged();
}

QmlOptionPLUS::QmlOptionPLUS(QObject *parent)
    : QmlOption(parent)
{}

QmlOptionPLUS::QmlOptionPLUS(QString label, int value, QObject *parent)
    : QmlOption(label, value, parent)
{}

QString QmlOptionPLUS::prova() const
{
    return m_prova;
}

void QmlOptionPLUS::setProa(const QString &newProva)
{
    if (m_prova == newProva)
        return;
    m_prova = newProva;
    emit provaChanged();
}

QString QmlOption::formattedLabel() const
{
    return m_formattedLabel;
}

void QmlOption::setFormattedLabel(const QString &newFormattedLabel)
{
    if (m_formattedLabel == newFormattedLabel)
        return;
    m_formattedLabel = newFormattedLabel;
    emit formattedLabelChanged();
}

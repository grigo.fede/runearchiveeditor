#include "../include/archiveEntry.h"
#include "QDebug"

ArchiveEntry::ArchiveEntry(QObject *parent)
    : QObject{parent}
    , m_name{""}
    , m_note{""}
    , m_category{""}
    , m_registrationCode{0}
    , m_copyNumber{0}

{}

ArchiveEntry::ArchiveEntry(QString code, QString name, QString note, QObject *parent)
    : QObject{parent}
    , m_name{name}
    , m_note{note}
    , m_category{""}
    , m_registrationCode{0}
    , m_copyNumber{0}
{
    this->parsePrintCode(code);
}

ArchiveEntry::ArchiveEntry(ArchiveEntry &other)
    : QObject{other.parent()}
    , m_name{other.name()}
    , m_note{other.note()}
    , m_category{other.category()}
    , m_registrationCode{other.registrationCode()}
    , m_copyNumber{other.copyNumber()}
{}

ArchiveEntry::ArchiveEntry(ArchiveEntry &&other)
    : QObject{other.parent()}
    , m_name{other.name()}
    , m_note{other.note()}
    , m_category{other.category()}
    , m_registrationCode{other.registrationCode()}
    , m_copyNumber{other.copyNumber()}
{}

ArchiveEntry &ArchiveEntry::operator=(ArchiveEntry &&other)
{
    if (this != &other) {
        m_name = other.name();
        m_note = other.note();
        m_category = other.category();
        m_registrationCode = other.registrationCode();
        m_copyNumber = other.copyNumber();
    }
    return *this;
}

QString ArchiveEntry::getPrintCode()
{
    QString result = m_category;
    int numberSize = QString::number(m_registrationCode).toStdString().length();

    QString tmpCode = "0000";
    for (int i = numberSize - 1; i >= 0; i--) {
        tmpCode[4 - numberSize + i] = QString::number(m_registrationCode).toStdString().at(i);
    }

    result += tmpCode + "." + QString::number(m_copyNumber);
    return result;
}

void ArchiveEntry::parsePrintCode(QString candidate)
{
    QStringList splited = candidate.split(".");
    if (splited.size() != 2)
        // throw Error here?
        return;
    if (splited[0].size() < 6)
        // throw Error here?
        return;
    QString codeStr = splited[0].right(4);
    QString category = splited[0].left(splited[0].size() - 4);
    setCategory(category);
    setCopyNumber(splited[1].toInt());
    setRegistrationCode(codeStr.toInt());
}

QString ArchiveEntry::category() const
{
    return m_category;
}

void ArchiveEntry::setCategory(const QString &newCategory)
{
    if (m_category == newCategory)
        return;
    m_category = newCategory;
    emit categoryChanged();
}

int ArchiveEntry::registrationCode() const
{
    return m_registrationCode;
}

void ArchiveEntry::setRegistrationCode(int newRegistrationCode)
{
    if (m_registrationCode == newRegistrationCode)
        return;
    m_registrationCode = newRegistrationCode;
    emit registrationCodeChanged();
}

QString ArchiveEntry::name() const
{
    return m_name;
}

void ArchiveEntry::setName(const QString &newName)
{
    if (m_name == newName)
        return;
    m_name = newName;
    emit nameChanged();
}

QString ArchiveEntry::note() const
{
    return m_note;
}

void ArchiveEntry::setNote(const QString &newNote)
{
    if (m_note == newNote)
        return;
    m_note = newNote;
    emit noteChanged();
}

int ArchiveEntry::copyNumber() const
{
    return m_copyNumber;
}

void ArchiveEntry::setCopyNumber(int newCopyNumber)
{
    if (m_copyNumber == newCopyNumber)
        return;
    m_copyNumber = newCopyNumber;
    emit copyNumberChanged();
}

bool ArchiveEntry::isCodeParsable(QString oldCode)
{
    if (oldCode != "" && oldCode.split(".").length() == 2) {
        QString code = oldCode.split(".")[0];
        QString copyNumber = oldCode.split(".")[1];
        if (code.size() < 6 || copyNumber.size() != 1)
            return false;

        bool codeOk, rnOk;
        QString codeRef = code.right(4);
        codeRef.toInt(&codeOk);
        copyNumber.toInt(&rnOk);
        if (codeOk && rnOk)
            return true;
    }
    return false;
}

ArchiveHistoryEntry::ArchiveHistoryEntry(QObject *parent)
    : ArchiveEntry(parent)
    , m_previousCode{""}
    , m_withLabel{false}
{}

ArchiveHistoryEntry::ArchiveHistoryEntry(QString oldCode, QObject *parent)
    : ArchiveEntry(parent)
    , m_previousCode{oldCode}
    , m_withLabel{false}
    , m_sheetName{""}

{
    if (isCodeParsable(oldCode)) {
        parsePrintCode(oldCode);
    } else {
        qDebug() << " The following code could not be parsed" << oldCode;
    }
}

ArchiveHistoryEntry::ArchiveHistoryEntry(QString code, QString name, QString note, QObject *parent)
    : ArchiveEntry(code, name, note, parent)
    , m_previousCode{code}
    , m_withLabel{false}
    , m_sheetName{""}

{}

ArchiveHistoryEntry::ArchiveHistoryEntry(ArchiveHistoryEntry &&other)
    : ArchiveEntry(other)
    , m_previousCode{other.previousCode()}
    , m_withLabel{other.withLabel()}
    , m_sheetName{""}

{}

ArchiveHistoryEntry::ArchiveHistoryEntry(ArchiveHistoryEntry &other)
    : ArchiveEntry(other)
    , m_previousCode{other.previousCode()}
    , m_withLabel{other.withLabel()}
    , m_sheetName{""}

{}

ArchiveHistoryEntry &ArchiveHistoryEntry::operator=(ArchiveHistoryEntry &&other)
{
    if (this != &other) {
        m_name = other.name();
        m_note = other.note();
        m_category = other.category();
        m_registrationCode = other.registrationCode();
        m_copyNumber = other.copyNumber();
        m_previousCode = other.previousCode();
        m_withLabel = other.withLabel();
        m_itemIndex = other.itemIndex();
        m_sheetName = other.sheetName();
    }
    return *this;
}

bool ArchiveHistoryEntry::operator==(const ArchiveHistoryEntry &&other) const
{
    return (m_name == other.name() && m_note == other.note() && m_category == other.category()
            && m_registrationCode == other.registrationCode() && m_copyNumber == other.copyNumber()
            && m_previousCode == other.previousCode() && m_withLabel == other.withLabel()
            && m_itemIndex == other.itemIndex() && m_sheetName == other.sheetName());
}

bool ArchiveHistoryEntry::isEqual(const ArchiveHistoryEntry &&other) const
{
    return (m_category == other.category() && m_registrationCode == other.registrationCode()
            && m_copyNumber == other.copyNumber());
}

QString ArchiveHistoryEntry::previousCode() const
{
    return m_previousCode;
}

void ArchiveHistoryEntry::setPreviousCode(const QString &newPreviousCode)
{
    if (m_previousCode == newPreviousCode)
        return;
    m_previousCode = newPreviousCode;
    emit PreviousCodeChanged();
}

bool ArchiveHistoryEntry::isValid()
{
    // if (m_previousCode == "")
    //     return false;
    if (m_sheetName == "")
        return false;
    return ArchiveEntry::isCodeParsable(getFullCode());
}

QString ArchiveHistoryEntry::getFullCode()
{
    return getPrintCode();
}

bool ArchiveHistoryEntry::withLabel() const
{
    return m_withLabel;
}

void ArchiveHistoryEntry::setWithLabel(bool newWithLabel)
{
    if (m_withLabel == newWithLabel)
        return;
    m_withLabel = newWithLabel;
    emit withLabelChanged();
}

int ArchiveHistoryEntry::itemIndex() const
{
    return m_itemIndex;
}

void ArchiveHistoryEntry::setItemIndex(int newItemIndex)
{
    if (m_itemIndex == newItemIndex)
        return;
    m_itemIndex = newItemIndex;
    emit itemIndexChanged();
}

QString ArchiveHistoryEntry::sheetName() const
{
    return m_sheetName;
}

void ArchiveHistoryEntry::setSheetName(const QString &newSheetName)
{
    if (m_sheetName == newSheetName)
        return;
    m_sheetName = newSheetName;
    emit sheetNameChanged();
}

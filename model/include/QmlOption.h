#ifndef QMLOPTION_H
#define QMLOPTION_H

#include <QObject>

class QmlOption : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString label READ label WRITE setLabel NOTIFY labelChanged FINAL)
    Q_PROPERTY(QString formattedLabel READ formattedLabel WRITE setFormattedLabel NOTIFY
                   formattedLabelChanged FINAL)
    Q_PROPERTY(int value READ value WRITE setValue NOTIFY valueChanged FINAL)
public:
    explicit QmlOption(QObject *parent = nullptr);
    explicit QmlOption(QString label, int value, QObject *parent = nullptr);

    QString label() const;
    void setLabel(const QString &newLabel);

    int value() const;
    void setValue(int newValue);

    QString formattedLabel() const;
    void setFormattedLabel(const QString &newFormattedLabel);

signals:
    void labelChanged();
    void valueChanged();

    void formattedLabelChanged();

private:
    QString m_label;
    int m_value;
    QString m_formattedLabel;
};

class QmlOptionPLUS : public QmlOption
{
    Q_OBJECT

    Q_PROPERTY(QString prova READ prova WRITE setProa NOTIFY provaChanged FINAL)

public:
    explicit QmlOptionPLUS(QObject *parent = nullptr);
    explicit QmlOptionPLUS(QString label, int value, QObject *parent = nullptr);

    QString prova() const;
    void setProa(const QString &newProva);

signals:

    void provaChanged();

private:
    QString m_prova;
};

#endif // QMLOPTION_H

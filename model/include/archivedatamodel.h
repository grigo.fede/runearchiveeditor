#ifndef ARCHIVEDATAMODEL_H
#define ARCHIVEDATAMODEL_H

#include <QList>
#include <QObject>
#include <QSortFilterProxyModel>
#include <QVariantList>
#include "QCustomListModel.h"
#include "QmlOption.h"
#include "archiveEntry.h"
#include "qtmetamacros.h"

class ArchiveDataModel : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QCustomListModelBase *items READ getItems NOTIFY itemsChanged)
    Q_PROPERTY(QCustomListModelBase *categories READ getCategories NOTIFY categoriesChanged)
    Q_PROPERTY(QCustomListModelBase *pageList READ pageList NOTIFY pageListChanged)
    Q_PROPERTY(QCustomListModelBase *selectionList READ selectionList NOTIFY
                   selectionListChanged)
    Q_PROPERTY(QString currentCategory READ currentCategory WRITE setCurrentCategory NOTIFY
                   currentCategoryChanged)
    Q_PROPERTY(QSortFilterProxyModel *proxyItems READ proxyItems WRITE setProxyItems NOTIFY
                   proxyItemsChanged)

public:
    explicit ArchiveDataModel(QObject *parent = nullptr);

    QCustomListModel<ArchiveHistoryEntry> *getItems();

    QCustomListModelBase *getCategories() const;

    Q_INVOKABLE void addNewCategory(QString label);
    Q_INVOKABLE void addNewItem(QString category, QString title, int code, int copy);
    void addNewItem(QString category, QString title, int code, int copy, QString page);

    ArchiveHistoryEntry *findByItemIndex(int itemIndex);
    ArchiveHistoryEntry *findByFullCode(QString code);
    void standardItemSort();

    QmlIntegerModelList *selectionList();

    QCustomListModel<QmlOption> *pageList() const;

    QString currentCategory() const;
    void setCurrentCategory(const QString &newCurrentCategory);

    QSortFilterProxyModel *proxyItems() const;
    void setProxyItems(QSortFilterProxyModel *newProxyItems);

signals:
    void itemsChanged(QCustomListModelBase *items);

    void categoriesChanged();

    void selectionListChanged();

    void pageListChanged();

    void currentCategoryChanged();

    void proxyItemsChanged();

public slots:
    void handleSelection(int i);

private:
    QCustomListModel<ArchiveHistoryEntry> m_items;
    QCustomListModel<QmlOption> *m_categories;
    QmlIntegerModelList *m_selectionList;
    QCustomListModel<QmlOption> *m_pageList;
    QString m_currentCategory;
    QSortFilterProxyModel *m_proxyItems;
};

#endif // ARCHIVEDATAMODEL_H

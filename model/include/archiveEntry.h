#ifndef ARCHIVEENTRY_H
#define ARCHIVEENTRY_H

#include <QObject>

class ArchiveEntry : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString category READ category WRITE setCategory NOTIFY categoryChanged)
    Q_PROPERTY(int registrationCode READ registrationCode WRITE setRegistrationCode NOTIFY
                   registrationCodeChanged)
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(QString note READ note WRITE setNote NOTIFY noteChanged)
    Q_PROPERTY(int copyNumber READ copyNumber WRITE setCopyNumber NOTIFY copyNumberChanged)

public:
    explicit ArchiveEntry(QObject *parent = nullptr);
    explicit ArchiveEntry(QString code, QString name, QString note, QObject *parent = nullptr);
    // copy constructor
    ArchiveEntry(ArchiveEntry &other);
    // moving constructor
    ArchiveEntry(ArchiveEntry &&other);
    // moving assignment
    ArchiveEntry &operator=(ArchiveEntry &&other);

    QString getPrintCode();
    void parsePrintCode(QString string);

    QString category() const;
    void setCategory(const QString &newCategory);

    int registrationCode() const;
    void setRegistrationCode(int newRegistrationCode);

    QString name() const;
    void setName(const QString &newName);

    QString note() const;
    void setNote(const QString &newNote);

    int copyNumber() const;
    void setCopyNumber(int newCopyNumber);

    static bool isCodeParsable(QString string);

signals:

    void categoryChanged();

    void registrationCodeChanged();

    void nameChanged();

    void noteChanged();

    void copyNumberChanged();

protected:
    QString m_category;
    int m_registrationCode;
    QString m_name;
    QString m_note;
    int m_copyNumber;
};

class ArchiveHistoryEntry : public ArchiveEntry
{
    Q_OBJECT
    Q_PROPERTY(
        QString previousCode READ previousCode WRITE setPreviousCode NOTIFY PreviousCodeChanged)
    Q_PROPERTY(bool withLabel READ withLabel WRITE setWithLabel NOTIFY withLabelChanged)
    Q_PROPERTY(int itemIndex READ itemIndex WRITE setItemIndex NOTIFY itemIndexChanged)
    Q_PROPERTY(QString sheetName READ sheetName WRITE setSheetName NOTIFY sheetNameChanged)

public:
    ArchiveHistoryEntry(QObject *parent = nullptr);
    ArchiveHistoryEntry(QString oldCode, QObject *parent = nullptr);
    ArchiveHistoryEntry(QString code, QString name, QString note, QObject *parent = nullptr);
    ArchiveHistoryEntry(ArchiveHistoryEntry &&other);
    ArchiveHistoryEntry(ArchiveHistoryEntry &other);
    ArchiveHistoryEntry &operator=(ArchiveHistoryEntry &&other);
    bool operator==(const ArchiveHistoryEntry &&other) const;

    bool isEqual(const ArchiveHistoryEntry &&other) const;

    QString previousCode() const;
    void setPreviousCode(const QString &newPreviousCode);

    Q_INVOKABLE bool isValid();
    Q_INVOKABLE QString getFullCode();

    bool withLabel() const;
    void setWithLabel(bool newWithLabel);

    int itemIndex() const;
    void setItemIndex(int newItemIndex);

    QString sheetName() const;
    void setSheetName(const QString &newSheetName);

signals:
    void PreviousCodeChanged();

    void withLabelChanged();

    void itemIndexChanged();

    void sheetNameChanged();

private:
    QString m_previousCode;
    bool m_withLabel;
    int m_itemIndex;
    QString m_sheetName;
};

#endif // ARCHIVEENTRY_H

#ifndef QCUSTOMLISTMODEL_H
#define QCUSTOMLISTMODEL_H

#include <QAbstractListModel>
#include <QMetaMethod>
#include <QMetaObject>
#include <QMetaProperty>
#include <QObject>
#include <QQmlEngine>
#include "qabstractitemmodel.h"
#include "qtmetamacros.h"

class QCustomListModelBase : public QAbstractListModel
{
    Q_OBJECT
public:
    explicit QCustomListModelBase(QObject *parent = nullptr)
        : QAbstractListModel(parent){};

    // QAbstractItemModel interface
public slots:
    virtual int rowCount(const QModelIndex &parent) const = 0;
    virtual int columnCount(const QModelIndex &parent) const = 0;
    virtual QVariant data(const QModelIndex &index, int role) const = 0;
    virtual QVariant headerData(int section, Qt::Orientation orientation, int role) const = 0;
    virtual QHash<int, QByteArray> roleNames() const = 0;
    virtual bool insertRows(int row, int count, const QModelIndex &parent) = 0;
    virtual bool removeRows(int row, int count, const QModelIndex &parent) = 0;
    virtual bool setData(const QModelIndex &index, const QVariant &value, int role) = 0;
    virtual Qt::ItemFlags flags(const QModelIndex &index) const = 0;

    // general pourpose interface
    virtual bool isEmpty(void) const = 0;
    virtual void append(QObject *item) = 0;
    virtual void insert(int idx, QObject *item) = 0;
    virtual void clear(void) = 0;
    virtual bool contains(QObject *item) const = 0;
    virtual int indexOf(QObject *item) const = 0;
    virtual void remove(int idx) = 0;
    virtual QObject *get(int idx) const = 0;
    virtual int size(void) const = 0;

protected slots:
    virtual void itemPropertyChangedHandler() = 0;
};

template<class T>
class QCustomListModel : public QCustomListModelBase
{
    static_assert(std::is_base_of<QObject, T>::value, "T must be a QObject or derived from QObject");

public:
    using const_iterator = typename QList<T *>::const_iterator;

    explicit QCustomListModel(QObject *parent = nullptr)
        : QCustomListModelBase(parent)
    {
        m_metaObj = T::staticMetaObject;

        m_handler = metaObject()->method(
            metaObject()->indexOfMethod("itemPropertyChangedHandler()"));
        const int len = m_metaObj.propertyCount();
        for (int propertyIdx = 0, role = (Qt::UserRole + 1); propertyIdx < len;
             ++propertyIdx, ++role) {
            QMetaProperty metaProp = m_metaObj.property(propertyIdx);
            const QByteArray propName = QByteArray(metaProp.name());
            m_roles.insert(role, propName);
            if (metaProp.hasNotifySignal()) {
                m_signalIdxToRole.insert(metaProp.notifySignalIndex(), role);
            }
        }
    };

    // QAbstractItemModel interface

    int rowCount(const QModelIndex &parent = QModelIndex()) const
    {
        Q_UNUSED(parent);
        return _data.size();
    };

    int columnCount(const QModelIndex &parent) const
    {
        Q_UNUSED(parent);
        return 1;
    };

    QVariant data(const QModelIndex &index, int role) const
    {
        if (!index.isValid())
            return QVariant();

        T *item = _data.at(index.row());
        QObject test;
        const QByteArray rolename = m_roles.value(role, QByteArrayLiteral(""));

        if (item != Q_NULLPTR && !rolename.isEmpty()) {
            const QMetaObject *metaObject = item->metaObject();

            if (role != Qt::UserRole)
                return item->property(rolename);
            else
                return QVariant::fromValue(static_cast<QObject *>(item));
        }
        return QVariant();
    }

    bool setData(const QModelIndex &index, const QVariant &value, int role)
    {
        if (!index.isValid() || index.row() < 0 || index.row() >= rowCount())
            return false;
        T *item = _data.at(index.row());

        if (item != Q_NULLPTR && role != Qt::UserRole
            && !m_roles.value(role, QByteArrayLiteral("")).isEmpty()) {
            return item->setProperty(m_roles.value(role, QByteArrayLiteral("")), value);
        }
        return false;
    }

    QVariant headerData(int section, Qt::Orientation orientation, int role) const
    {
        if (role == Qt::DisplayRole) {
            if (orientation == Qt::Horizontal) {
                // Restituisci i dati degli header delle colonne
                return QString("Column %1").arg(section + 1);
            } else if (orientation == Qt::Vertical) {
                // Restituisci i dati degli header delle righe
                return QString("Row %1").arg(section + 1);
            }
        } else {
            if (m_roles.contains(role)) {
                return m_roles.value(role);
            }
        }
        return QVariant();
    };

    bool insertRows(int row, int count, const QModelIndex &parent)
    {
        beginInsertRows(QModelIndex(), row, rowCount() + count - 1);

        endInsertRows();
        return true;
    };

    bool removeRows(int row, int count, const QModelIndex &parent)
    {
        beginRemoveRows(QModelIndex(), row, row + count - 1);
        _data.removeAt(row);

        endRemoveRows();
        return true;
    };

    Qt::ItemFlags flags(const QModelIndex &index) const
    {
        if (!index.isValid())
            return Qt::NoItemFlags;

        // Allow items to be enabled, selectable, and editable
        return Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsEditable;
    }

    ;
    QHash<int, QByteArray> roleNames() const { return m_roles; };

    // QCustomListModelBase interface
public:
    bool isEmpty() const { return _data.isEmpty(); };
    void append(QObject *item) Q_DECL_FINAL { append(qobject_cast<T *>(item)); }
    void append(T *item)
    {
        if (item != nullptr) {
            const int pos = _data.count();
            beginInsertRows(QModelIndex(), pos, pos);
            _data.append(item);
            referenceItem(item);
            endInsertRows();
        }
    };
    void append(const QList<T *> &itemList)
    {
        if (!itemList.isEmpty()) {
            const int pos = _data.count();
            beginInsertRows(QModelIndex(), pos, pos + itemList.count() - 1);
            _data.reserve(_data.count() + itemList.count());
            _data.append(itemList);
            const QList<T *> list{itemList};
            for (T *item : list) {
                if (item) {
                    referenceItem(item);
                }
            }
            endInsertRows();
        }
    }

    void insert(int idx, QObject *item)
    {
        if (item != nullptr) {
            beginInsertRows(QModelIndex(), idx, idx);
            _data.insert(idx, dynamic_cast<T *>(item));
            referenceItem(dynamic_cast<T *>(item));
            endInsertRows();
        }
    };
    void remove(int idx)
    {
        if (idx >= 0 && idx < _data.size()) {
            beginRemoveRows(QModelIndex(), idx, idx);
            T *item = _data.takeAt(idx);
            dereferenceItem(item);
            endRemoveRows();
        }
    };
    void clear()
    {
        if (isEmpty()) {
            return;
        }
        beginRemoveRows(QModelIndex(), 0, _data.count() - 1);

        for (int i = 0; i < _data.size(); ++i) {
            if (_data.at(i) != nullptr) {
                dereferenceItem(_data.at(i));
            }
        }
        _data.clear();
        endRemoveRows();
    };
    bool contains(QObject *item) const { return _data.contains(item); };
    int indexOf(QObject *item) const { return _data.indexOf(item); };

    QObject *get(int idx) const
    {
        if (idx >= 0 && idx < rowCount()) {
            return _data.value(idx);
        }
        return nullptr;
    };

    int size() const { return _data.size(); }

    const_iterator begin(void) const { return _data.begin(); }

    const_iterator end(void) const { return _data.end(); }
    typename QList<T *>::iterator begin(void) { return _data.begin(); }
    typename QList<T *>::iterator end(void) { return _data.end(); }

protected:
    void referenceItem(T *item)
    {
        if (item != nullptr) {
            if (item->parent() == nullptr) {
                item->setParent(this);
                QQmlEngine::setObjectOwnership(this, QQmlEngine::CppOwnership);
            }
            for (QHash<int, int>::const_iterator it = m_signalIdxToRole.constBegin();
                 it != m_signalIdxToRole.constEnd();
                 ++it) {
                auto tt = item->metaObject()->method(it.key());
                connect(item,
                        item->metaObject()->method(it.key()),
                        this,
                        m_handler,
                        Qt::UniqueConnection);
            }
        }
    }

    void dereferenceItem(T *item)
    {
        if (item != nullptr) {
            disconnect(this, nullptr, item, nullptr);
            disconnect(item, nullptr, this, nullptr);
            if (item->parent() == this) {
                item->deleteLater();
            }
        }
    }

    void itemPropertyChangedHandler()
    {
        T *item = qobject_cast<T *>(sender());
        int idx = _data.indexOf(item);
        int signal = senderSignalIndex();
        int role = m_signalIdxToRole.value(signal, -1);
        if (idx >= 0 && role >= 0) {
            const QModelIndex index = QAbstractListModel::index(idx, 0, QModelIndex());
            QVector<int> rolesList;
            rolesList.append(role);
            emit dataChanged(index, index, rolesList);
        }
    }

protected:
    QList<T *> _data;

private:
    QMetaObject m_metaObj;
    QMetaMethod m_handler;
    QHash<int, QByteArray> m_roles;
    QHash<int, int> m_signalIdxToRole;
};

class QmlInteger : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int value READ value WRITE setValue NOTIFY valueChanged)

public:
    QmlInteger(QObject *parent = nullptr)
        : QObject(parent)
        , m_value{0}
    {}
    QmlInteger(int value, QObject *parent = nullptr)
        : QObject(parent)
        , m_value{value}
    {}

    int value() const { return m_value; };
    void setValue(int newValue)
    {
        if (m_value == newValue)
            return;
        m_value = newValue;
        emit valueChanged();
    };
signals:
    void valueChanged();

private:
    int m_value;
};

//typedef QCustomListModel<QmlInteger> QmlIntegerModelList;

class QmlIntegerModelList : public QCustomListModel<QmlInteger>
{
    Q_OBJECT

public:
    explicit QmlIntegerModelList(QObject *parent = nullptr)
        : QCustomListModel<QmlInteger>(parent)
    {}

    using QCustomListModel<QmlInteger>::append;
    using QCustomListModel<QmlInteger>::contains;

    QList<int> toList() const
    {
        QList<int> ret;
        for (const QmlInteger *item : _data) {
            ret.append(item->value());
        }
        return ret;
    }

public slots:

    bool contains(int value)
    {
        for (const QmlInteger *item : _data) {
            if (item->value() == value) {
                return true;
            }
        }
        return false;
    }

    void append(int value)
    {
        QmlInteger *item = new QmlInteger(value);
        QCustomListModel<QmlInteger>::append(item);
    }

    void remove(int value)
    {
        for (int i = 0; i < _data.size(); ++i) {
            if (_data.at(i)->value() == value) {
                QCustomListModel<QmlInteger>::remove(i);
                return;
            }
        }
    }
};

#endif // QCUSTOMLISTMODEL_H
